-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para testing_fullstack
DROP DATABASE IF EXISTS `testing_fullstack`;
CREATE DATABASE IF NOT EXISTS `testing_fullstack` /*!40100 DEFAULT CHARACTER SET latin2 */;
USE `testing_fullstack`;

-- Volcando estructura para tabla testing_fullstack.caracteristica
DROP TABLE IF EXISTS `caracteristica`;
CREATE TABLE IF NOT EXISTS `caracteristica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `construccion_id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `caracteristica_construccion_id_foreign` (`construccion_id`),
  CONSTRAINT `caracteristica_construccion_id_foreign` FOREIGN KEY (`construccion_id`) REFERENCES `construccion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Caracteristicas de la construcción';

-- Volcando datos para la tabla testing_fullstack.caracteristica: ~4 rows (aproximadamente)
DELETE FROM `caracteristica`;
/*!40000 ALTER TABLE `caracteristica` DISABLE KEYS */;
INSERT INTO `caracteristica` (`id`, `construccion_id`, `nombre`, `descripcion`, `updated_at`, `created_at`) VALUES
	(1, 1, 'casa grande modificacion', 'La descripcion de la casa grande de Daniel', '2018-08-28 11:58:19', '2018-08-28 11:58:21'),
	(2, 1, 'Color gris', 'La fachada de la casa es de color gris', '2018-08-28 11:59:03', '2018-08-28 11:59:04'),
	(3, 2, 'casa de dos pisos modifi', 'La casa cuenta con la construccion en dos pisos', '2018-08-28 11:59:45', '2018-08-28 11:59:46'),
	(4, 2, 'Nueva caracteristica de la construccion 2', 'Descripción de la nueva caracteristica de la constriccion 2', '2018-08-28 14:22:56', '2018-08-28 14:22:56'),
	(5, 1, 'caracteristica nueva', 'es  una prueba mas', '2018-08-28 18:51:36', '2018-08-28 18:51:36');
/*!40000 ALTER TABLE `caracteristica` ENABLE KEYS */;

-- Volcando estructura para tabla testing_fullstack.construccion
DROP TABLE IF EXISTS `construccion`;
CREATE TABLE IF NOT EXISTS `construccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `clave` varchar(12) NOT NULL,
  `delegacion` varchar(50) NOT NULL,
  `colonia` varchar(100) NOT NULL,
  `calle` varchar(200) NOT NULL,
  `latitud` varchar(25) NOT NULL,
  `longitud` varchar(25) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Almacenara las construcciones';

-- Volcando datos para la tabla testing_fullstack.construccion: ~3 rows (aproximadamente)
DELETE FROM `construccion`;
/*!40000 ALTER TABLE `construccion` DISABLE KEYS */;
INSERT INTO `construccion` (`id`, `nombre`, `clave`, `delegacion`, `colonia`, `calle`, `latitud`, `longitud`, `created_at`, `modified_at`) VALUES
	(1, 'Casa Toluca', 'PCOM-fga/23', 'Lerma', 'Cedros 4000', 'Paseo del pino', '19.3123868', '-99.5403776', '2018-08-27 16:31:29', '2018-08-27 16:31:29'),
	(2, 'Casa Ecatepec', 'PCOM-ATL/56', 'Ecatepec de Morelos', 'Ciudad Cuauhtemoc', 'Atlaovalco', '19.6456401', '-99.0009141', '2018-08-28 03:13:20', '2018-08-28 03:13:20'),
	(14, 'Plaza sendero', 'PCOM-tgt/32', 'lerma', 'lerma', 'lerma', '19.2903985', '-99.5556439', '2018-08-28 21:01:15', '2018-08-28 21:01:15');
/*!40000 ALTER TABLE `construccion` ENABLE KEYS */;

-- Volcando estructura para tabla testing_fullstack.galeria
DROP TABLE IF EXISTS `galeria`;
CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `construccion_id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `ruta` varchar(500) NOT NULL,
  `latitud` varchar(50) NOT NULL,
  `longitud` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `galeria_construccion_id_foreign` (`construccion_id`),
  CONSTRAINT `galeria_construccion_id_foreign` FOREIGN KEY (`construccion_id`) REFERENCES `construccion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Galeria de localidades cercanas a la ubicación de la constru';

-- Volcando datos para la tabla testing_fullstack.galeria: ~7 rows (aproximadamente)
DELETE FROM `galeria`;
/*!40000 ALTER TABLE `galeria` DISABLE KEYS */;
INSERT INTO `galeria` (`id`, `construccion_id`, `nombre`, `ruta`, `latitud`, `longitud`, `created_at`, `modified_at`) VALUES
	(1, 1, 'Farmacia Guadalajara', 'http://www.mexlead.com.mx/imagenes/farmaciaGuadalajara.jpg', '19.3122571', '-99.5422483', '2018-08-28 16:07:24', '2018-08-28 16:07:24'),
	(2, 1, 'Salon la finca', 'http://www.mexlead.com.mx/imagenes/salonFinca.jpg', '19.3110179', '-99.542692', '2018-08-28 16:09:54', '2018-08-28 16:09:54'),
	(3, 2, 'Bodega Soriana', 'http://www.mexlead.com.mx/imagenes/bodegaAurrera.jpg', '19.6451652', '-99.0020192', '2018-08-28 18:59:12', '2018-08-28 18:59:12'),
	(5, 14, 'Clínica Dental Smile & Smile', 'http://www.mexlead.com.mx/imagenes/construccion.jpg', '19.290699005127', '-99.555404663086', '2018-08-28 21:01:15', '2018-08-28 21:01:15'),
	(6, 14, 'big churro', 'http://www.mexlead.com.mx/imagenes/construccion.jpg', '19.290330744915', '-99.555947579235', '2018-08-28 21:01:15', '2018-08-28 21:01:15'),
	(7, 14, 'Potzolcalli', 'http://www.mexlead.com.mx/imagenes/construccion.jpg', '19.290676449805', '-99.555398860349', '2018-08-28 21:01:15', '2018-08-28 21:01:15'),
	(8, 14, 'Tendencia', 'http://www.mexlead.com.mx/imagenes/construccion.jpg', '19.290705346562', '-99.555513885956', '2018-08-28 21:01:15', '2018-08-28 21:01:15');
/*!40000 ALTER TABLE `galeria` ENABLE KEYS */;

-- Volcando estructura para tabla testing_fullstack.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `authKey` varchar(255) NOT NULL,
  `accessToken` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin2;

-- Volcando datos para la tabla testing_fullstack.user: ~3 rows (aproximadamente)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `firstName`, `lastName`, `email`, `password`, `username`, `authKey`, `accessToken`) VALUES
	(1, 'Nathan', 'Smith', 'pcom@gmail.com', '3dd14afc9f2da6c03c4f6599553a4597', '', '', ''),
	(2, 'Daniel', 'Nava', 'danielnn83@yahoo.com.mx', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin21232f297a57a5a743894a0e4a801fc3', 'admin21232f297a57a5a743894a0e4a801fc321232f297a57a5a743894a0e4a801fc3'),
	(3, 'Ana Maria', 'Arzate', 'anamaria170783@yahoo.com.mx', '6238cb036a4229f33c2aac007631f50a', 'anamaria', 'a90f12896dad7e9dd9838b0ab30016ada90f12896dad7e9dd9838b0ab30016ad', 'a90f12896dad7e9dd9838b0ab30016adEXAMENa90f12896dad7e9dd9838b0ab30016ad');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
