<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use isudakoff\authclient\FoursquareApi;
use app\models\Construccion;
use app\models\Galeria;
use app\models\ConstruccionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConstruccionController implements the CRUD actions for Construccion model.
 */
class ConstruccionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['delete',"update","create"],
                'rules' => [
                    [
                        'actions' => ['delete',"update","create"],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Construccion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConstruccionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        foreach ($dataProvider->getModels() as &$data)
        {
            $c = "";
            foreach($data->caracteristicas as $caract )
                $c = $c . $caract->nombre . "<br />";
            if($c === "")
                $c = "No hay caracteristicas";
            $data->caracteristicasTexto = $c;
            
            $g = "";
            foreach($data->galerias as $gal )
                $g = $g . $gal->nombre . "<br />";
            if($g === "")
                $g = "No hay ubicaciones";
            $data->galeriasTexto = $g;
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     /**
     * Lists all Construccion models en un mapa.
     * @return mixed
     */
    public function actionMaps()
    {
        
        $dataProvider = Construccion::find()->indexBy('id')->all();
        
        return $this->render('maps', [
            
            'dataProvider' => $dataProvider,
        ]);
    }
    

    /**
     * Displays a single Construccion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Construccion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Construccion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->registrarLugaresCercanos($model->id);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model

        ]);
    }

    /**
     * Updates an existing Construccion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Construccion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Construccion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Construccion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Construccion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('La página requerida no existe.');
    }
    
    protected function registrarLugaresCercanos($idConstruccion)
    {
            $construccion = Construccion::findOne($idConstruccion);

            // Set your client key and secret
            $client_key = "FKLUBMKUFN5F4EXYEUFBQS4AMESPXXPRTC3W2WQACSJGNJSU";
            $client_secret = "QOZ35KDDACSV31CPT4X324JVHSY1GUWROC5PYWEDUDKUTZY1";
            // Load the Foursquare API library
                if($client_key=="" or $client_secret=="")
                {
                    echo 'Load client key and client secret from <a href="https://developer.foursquare.com/">foursquare</a>';
                    exit;
                }
            $foursquare = new FoursquareApi($client_key,$client_secret);
                //$location = array_key_exists("location",$_GET) ? $_GET['location'] : "Montreal, QC";
                //list($lat,$lng) = $foursquare->GeoLocate($location);
            // Prepare parameters
            $lat= $construccion->latitud;
            $lng = $construccion->longitud;
            $params = array("ll"=>"$lat,$lng");
            // Perform a request to a public resource
            $response = $foursquare->GetPublic("venues/search",$params);
            //$response = $foursquare->GetPublic("venues/4b034dbff964a520744e22e3/photos");
            $venues = $foursquare->getResponseFromJsonString($response);
            //$venues = json_decode($response);

        $listaLugares = array();
        foreach($venues as $key => $r)
        {
            for($i = 0; $i < count($r); $i++)
            {
                $arregloLugares = (array)$r[$i];
                $reg = array();
                foreach($arregloLugares as $key => $datos)
                {     
                    switch($key)
                    {
                        case "id":
                        case "name":
                            $reg[$key]=$datos;
                            break;
                        case "location":
                           $ubicacionCompleta = (array)$datos;
                            foreach($ubicacionCompleta as $k => $ubicacion)
                            {
                                switch($k)
                                {
                                    case "address":
                                    case "lat":
                                    case "lng":
                                        $reg[$k]=$ubicacion;
                                        break;
                                }
                            }
                        break;
                    }

                }
                $listaLugares[] = $reg;

            }
        }
        //Sacaremos las fotos
        //Ya teniendo los lugares ingresaremos los primeros 5
        $contador = 0;
        
        foreach($listaLugares as &$lugar)    
        {
            $model = new Galeria();
            foreach($lugar as $key => $valor)
            {
               switch($key)
                {
                   case "id":
                       //echo $valor . "    ";
                       /* $response = $foursquare->GetPublic("venues/".$valor."/photos");
                        print_r($response);
                        $fotos = $foursquare->getResponseFromJsonString($response);
                        print_r($fotos);
                        exit;
                        */
                        //$lugar["fotos"] = $fotos;
                       break;
                   case "name":
                       $model->nombre = "".$valor;
                       break;
                   case "lat":
                       $model->latitud = "".$valor;
                       break;
                   case "lng":
                       $model->longitud = "".$valor;
                       break;
                }
            }
            //IMAGEN GENERICA DEBIDO A QUE NOS REESTRINGIO ESA PARTE FOURSQUARE
            //Por que, Foursquare me indico cuota excedida con el siguiente error
            //{"meta":{"code":429,"errorType":"quota_exceeded","errorDetail":"Quota exceeded","requestId":"5b863c34dd57972c545faacb"},
            //"response":{}}stdClass Object ( )
                $model->ruta = "http://www.mexlead.com.mx/imagenes/construccion.jpg";
                $model->construccion_id = $construccion->id;
                
                if ($model->save()) {
                    Yii::debug("Alta de lugar cercano " . $model->nombre);
                }else
                {
                    $errores = $model->errors;
                    print_r($errores);
                    exit;
                }
            if($contador >= 4)
                break;
            $contador++;
        }
    }
}
