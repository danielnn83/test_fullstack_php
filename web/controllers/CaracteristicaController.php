<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Caracteristica;
use app\models\Construccion;
use app\models\CaracteristicaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CaracteristicaController implements the CRUD actions for Caracteristica model.
 */
class CaracteristicaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['delete',"update","create"],
                'rules' => [
                    [
                        'actions' => ['delete',"update","create"],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Caracteristica models.
     * @return mixed
     */
    public function actionIndex($idConstruccion = 0)
    {
        if(isset($idConstruccion))
        {
            $searchModel = new CaracteristicaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere('construccion_id = '.$idConstruccion);
            $construccion = Construccion::findOne($idConstruccion);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            "construccion" => $construccion
        ]);
        
    }

    /**
     * Displays a single Caracteristica model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Caracteristica model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idConstruccion = null)
    {
        
        $model = new Caracteristica();
        $construccion = Construccion::findOne($idConstruccion);
        $model->construccion_id = $idConstruccion;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            "construccion" => $construccion
        ]);
    }

    /**
     * Updates an existing Caracteristica model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
       ]);
    }

    /**
     * Deletes an existing Caracteristica model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index',"idConstruccion"=>$model->construccion_id]);
    }

    /**
     * Finds the Caracteristica model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Caracteristica the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Caracteristica::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
