<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "galeria".
 *
 * @property int $id
 * @property int $construccion_id
 * @property string $nombre
 * @property string $ruta
 * @property string $latitud
 * @property string $longitud
 * @property string $created_at
 * @property string $modified_at
 *
 * @property Construccion $construccion
 */
class Galeria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'galeria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['construccion_id', 'nombre', 'ruta', 'latitud', 'longitud'], 'required'],
            [['construccion_id'], 'integer'],
           // [['created_at', 'modified_at'], 'safe'],
            [['nombre'], 'string', 'max' => 100],
            [['ruta'], 'string', 'max' => 500],
            [['latitud', 'longitud'], 'string', 'max' => 50],
            [['construccion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Construccion::className(), 'targetAttribute' => ['construccion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'construccion_id' => 'Construccion ID',
            'nombre' => 'Nombre del lugar',
            'ruta' => 'Ruta',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConstruccion()
    {
        return $this->hasOne(Construccion::className(), ['id' => 'construccion_id']);
    }
}
