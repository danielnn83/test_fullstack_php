<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "caracteristica".
 *
 * @property int $id
 * @property int $construccion_id
 * @property string $nombre
 * @property string $descripcion
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Construccion $construccion
 */
class Caracteristica extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'caracteristica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['construccion_id', 'nombre', 'descripcion'], 'required'],
            [['construccion_id'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['nombre'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 500],
            [['construccion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Construccion::className(), 'targetAttribute' => ['construccion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'construccion_id' => 'Construccion ID',
            'nombre' => 'Nombre de la caracteristica',
            'descripcion' => 'Descripcion',
            'updated_at' => 'Ultima actualización',
            'created_at' => 'Creado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConstruccion()
    {
        return $this->hasOne(Construccion::className(), ['id' => 'construccion_id']);
    }
}
