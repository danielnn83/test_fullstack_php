<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "construccion".
 *
 * @property int $id
 * @property string $nombre
 * @property string $clave
 * @property string $delegacion
 * @property string $colonia
 * @property string $calle
 * @property string $latitud
 * @property string $longitud
 * @property string $created_at
 * @property string $modified_at
 */
class Construccion extends \yii\db\ActiveRecord
{
    
    public $caracteristicasTexto;
    public $galeriasTexto;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'construccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'clave', 'delegacion', 'colonia', 'calle', 'latitud', 'longitud'], 'required', 'message' => 'Campo obligatorio.'],
            [['created_at', 'modified_at'], 'safe'],
            [['nombre', 'colonia'], 'string', 'max' => 100],
            [['clave'], 'string', 'max' => 11],
            [['delegacion'], 'string', 'max' => 50],
            [['calle'], 'string', 'max' => 200],
            [['latitud', 'longitud'], 'string', 'max' => 25],
            ['nombre', 'match', 'pattern'=>'/^[a-zA-ZÁÉÍÓÚÑáéíóúñ\s]+$/i', 'message'=>Yii::t('app','Sólo se permite texto.')],
            ['clave', 'match', 'pattern'=>'/^PCOM-[a-zA-ZÁÉÍÓÚÑáéíóúñ]{3}\/[0-9]{2}$/i', 'message'=>Yii::t('app','El formato es PCOM-XXX/##, donde XXX son letras y ## números')],
        ];
    }
    

    public function getCaracteristicas()
    {
       // $listaCaracteristicas = Caracteristica::findAll([
         //   "construccion_id"=>$this->id]);
        $listaCaracteristicas = $this->hasMany(Caracteristica::className(), ['construccion_id' => 'id']);
                
        return $listaCaracteristicas;
    }


    public function getGalerias()
    {
      
        $listaGalerias = $this->hasMany(Galeria::className(), ['construccion_id' => 'id']);
                
        return $listaGalerias;
    }    
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'clave' => 'Clave',
            'delegacion' => 'Delegación',
            'colonia' => 'Colonia',
            'calle' => 'Calle',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'created_at' => 'Creado',
            'modified_at' => 'Ultima actualización',
        ];
    }
}
