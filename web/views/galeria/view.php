<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Galeria */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Construcciones', 'url' => ['/construccion/index']];
$this->params['breadcrumbs'][] = ['label' => $model->construccion->nombre, 'url' => ['/construccion/view', 'id' => $model->construccion->id]];
$this->params['breadcrumbs'][] = ['label' => 'Galerias', 'url' => ['index',"idConstruccion"=>$model->construccion->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="galeria-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro de borrar esta ubicación '.$model->nombre.' ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'construccion_id',
            'nombre',
            'ruta',
            'latitud',
            'longitud',
            'created_at',
            'modified_at',
        ],
    ]) ?>

</div>
