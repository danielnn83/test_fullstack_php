<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GaleriaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ubicaciones cercanas de la construcción ' . Html::encode($construccion->nombre);
$this->params['breadcrumbs'][] = ['label' => 'Construcciones', 'url' => ['/construccion/index']];
$this->params['breadcrumbs'][] = ['label' => $construccion->nombre, 'url' => ['/construccion/view', 'id' => $construccion->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="galeria-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Agregar ubicación con fotografía', ['create',"idConstruccion"=>$construccion->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'construccion_id',
            'nombre',
            'ruta',
            'latitud',
            'longitud',
            //'created_at',
            //'modified_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
