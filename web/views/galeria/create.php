<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Galeria */

$this->title = 'Agregar ubicación con fotografía';
$this->params['breadcrumbs'][] = ['label' => 'Construcciones', 'url' => ['/construccion/index']];
$this->params['breadcrumbs'][] = ['label' => $construccion->nombre, 'url' => ['/construccion/view', 'id' => $construccion->id]];
$this->params['breadcrumbs'][] = ['label' => 'Galerias', 'url' => ['index',"idConstruccion"=>$construccion->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="galeria-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
