<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Galeria */

$this->title = 'Actualizar ubicación con fotografía: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Construcciones', 'url' => ['/construccion/index']];
$this->params['breadcrumbs'][] = ['label' => $model->construccion->nombre, 'url' => ['/construccion/view', 'id' => $model->construccion->id]];
$this->params['breadcrumbs'][] = ['label' => 'Galerias', 'url' => ['index',"idConstruccion"=>$model->construccion->id]];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="galeria-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
