<?php

/* @var $this yii\web\View */

$this->title = 'Inmobiliaria Propiedades';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Examen Práctico: Software Engineer!</h2>

        <p class="lead">M.I. DANIEL NAVA NAVA</p>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h3>USUARIOS</h3>

                <p><ul><li>Permitir el registro y autenticación de usuarios.</li></ul></p>

                
            </div>
            <div class="col-lg-4">
                <h3>CONSTRUCCIONES</h3>

                <ul><li>Nombre de la construcción: validar solo el uso de textos</li>
                   <li>Clave de la construcción: respetar el siguiente formato PCOM-XXX/##</li>
                   <li>Galeria de imagenes: deberá consumir el API de Foursquare (en php) para obtener 5 fotos de localidades cercanas a la ubicación de la construcción y almacenarlas como parte de su la galería</li>
                   <li>Datos de ubicación mínimos requeridos: Delegación, Colonia, calle</li>
                   <li>Datos de geoposicionamiento: latitud y longitud</li>
                   <li>Características: Se puede agregar N características de forma dinámica</li>
                </ul>
            </div>
            <div class="col-lg-4">
                <h3>API DE GOOGLE</h3>

                <p><ul>
                    <li>Crea un mapa con el API de google maps donde se visualicen los pins de las construcciones registradas</li>
                    <li>Al seleccionar cualquier pin del mapa; debe mostrar el nombre de la construcción y desplegar una galería responsive de sus fotos almacenadas</li>
                    <li>Al terminar de cargar la galeria; agregar un link que permita mostrar los detalles de construction (Nombre, Clave, Caracteristicas, etc.)</li>
                   </ul></p>

                
            </div>
        </div>

    </div>
</div>
