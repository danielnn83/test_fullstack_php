<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Caracteristica */

$this->title = 'Actualizar Característica: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Construcciones', 'url' => ['/construccion/index']];
$this->params['breadcrumbs'][] = ['label' => $model->construccion->nombre, 'url' => ['/construccion/view', 'id' => $model->construccion->id]];
$this->params['breadcrumbs'][] = ['label' => 'Caracteristicas', 'url' => ['index',"idConstruccion"=>$model->construccion->id]];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="caracteristica-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
