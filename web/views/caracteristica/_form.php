<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Caracteristica */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="caracteristica-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'construccion_id')->hiddenInput(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'updated_at')->textInput() ?-->

    <!--?= $form->field($model, 'created_at')->textInput() ?-->

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
