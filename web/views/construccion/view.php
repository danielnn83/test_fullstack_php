<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;

/* @var $this yii\web\View */
/* @var $model app\models\Construccion */


$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Construcciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="construccion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro de borrar la construcción' . Html::encode($model->nombre) ."?",
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Ver caracteristicas', ['/caracteristica/index', 'idConstruccion' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Ver ubicaciones cercanas', ['/galeria/index', 'idConstruccion' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'nombre',
            'clave',
            'delegacion',
            'colonia',
            'calle',
            'latitud',
            'longitud',
            'created_at',
            'modified_at',
        ],
    ]) ?>

</div>
<?php
echo '<h3>Ubicación de la Construcción</h3>';

$coord = new LatLng(['lat' => Html::encode($model->latitud), 'lng' => Html::encode($model->longitud)]);
$map = new Map([
    'center' => $coord,
    'zoom' => 17,
    'width' => 'auto',
    'height' => 480,
]);
 
$imagen ='http://maps.google.com/mapfiles/kml/pal2/icon10.png';

$marker = new Marker([
    'position' => $coord,
    'title' => Html::encode($model->nombre),
    'icon'=>$imagen
]);
                    $imagenesP = "";
                    $imagenesN = "";
                    $contador = 0;
                    foreach($model->galerias as $m)
                    {
                        $imagenesP = $imagenesP . '<li class="col-sm-4">'.Html::encode($m->nombre).'
                            <a class="thumbnail" id="carousel-selector-'.$contador.'">
                                <img src="'.Html::encode($m->ruta).'">
                            </a>
                        </li>';
                        if($contador == 0)
                        {
                            $imagenesN = $imagenesN . '<div class="active item" data-slide-number="'.$contador.'">
                                            <img src="'.Html::encode($m->ruta).'"><H4>'.Html::encode($m->nombre).'</H4></div>';
                        }else
                        {
                            $imagenesN = $imagenesN . '<div class="item" data-slide-number="'.$contador.'">
                                            <img src="'.Html::encode($m->ruta).'"><H4>'.Html::encode($m->nombre).'</H4></div>';
                        }
                        $contador++;
                    }

$marker->attachInfoWindow(
    new InfoWindow([
        'content' => '<p><h3>LUGARES CERCANOS A LA CONSTRUCCION '.Html::encode($model->nombre).'</h3></p>'
        . '<div class="container">
    <div id="main_area">
        <!-- Slider -->
        <div class="row">
            <div class="col-sm-4" id="slider-thumbs">
                <!-- Bottom switcher of slider -->
                <ul class="hide-bullets">'.$imagenesP.'
                </ul>
            </div>
            <div class="col-sm-6">
                <div class="col-xs-12" id="slider">
                    <!-- Top part of the slider -->
                    <div class="row">
                        <div class="col-sm-12" id="carousel-bounding-box">
                            <div class="carousel slide" id="myCarousel">
                                <!-- Carousel items -->
                                <div class="carousel-inner">
                                    '.$imagenesN.'
                                </div>
                                <!-- Carousel nav -->
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Slider-->
        </div>

    </div>
</div>'
        .Html::a('VER TODA LA INFORMACION COMPLETA DE LA CONSTRUCCION', ['view', 'id' => $model->id])
    ])
);
$map->addOverlay($marker);
$imagenLugaresCercanos = "http://maps.google.com/mapfiles/kml/pal2/icon13.png";

foreach($model->galerias as $m)
{    
    $coordx = new LatLng(['lat' => Html::encode($m->latitud), 'lng' => Html::encode($m->longitud)]);
    
    $marker = new Marker([
        'position' => $coordx,
        'title' => Html::encode($m->nombre),
        'icon'=>$imagenLugaresCercanos    
    ]);

    $marker->attachInfoWindow(
        new InfoWindow([
            'content' => '<p>'.Html::encode($m->nombre).'</p><br />'.Html::a('Ver información', ['view', 'id' => $m->id])
        ])
    );

    $map->addOverlay($marker);
}

 


// Display the map -finally :)
echo $map->display();
?>
<script>
  jQuery(document).ready(function($) {
 
        $('#myCarousel').carousel({
                interval: 500
        });
 
        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click(function () {
        var id_selector = $(this).attr("id");
        try {
            var id = /-(\d+)$/.exec(id_selector)[1];
            console.log(id_selector, id);
            jQuery('#myCarousel').carousel(parseInt(id));
        } catch (e) {
            console.log('Regex failed!', e);
        }
    });
        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
                 var id = $('.item.active').data('slide-number');
                $('#carousel-text').html($('#slide-content-'+id).html());
        });
});

</script>