<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<?php

use yii\helpers\Html;

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConstruccionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Construcciones (Mapa)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="construccion-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Agregar Construcción', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Ver lista de las construcciones', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

   

<?php
   
foreach($dataProvider as $data)
{    
    $model = $data;
    
    break;
}

$coord = new LatLng(['lat' => Html::encode($model->latitud), 'lng' => Html::encode($model->longitud)]);
$imagen ='http://maps.google.com/mapfiles/kml/pal2/icon10.png';


$map = new Map([
    'center' => $coord,
    'zoom' => 10,
    'width' => 'auto',
    'height' => 480,
]);
 
foreach($dataProvider as $model)
{   
                    $imagenesP = "";
                    $imagenesN = "";
                    $contador = 0;
                    foreach($model->galerias as $m)
                    {
                        $imagenesP = $imagenesP . '<li class="col-sm-4">'.Html::encode($m->nombre).'
                            <a class="thumbnail" id="carousel-selector-'.$contador.'">
                                <img src="'.Html::encode($m->ruta).'">
                            </a>
                        </li>';
                        if($contador == 0)
                        {
                            $imagenesN = $imagenesN . '<div class="active item" data-slide-number="'.$contador.'">
                                            <img src="'.Html::encode($m->ruta).'"><H4>'.Html::encode($m->nombre).'</H4></div>';
                        }else
                        {
                            $imagenesN = $imagenesN . '<div class="item" data-slide-number="'.$contador.'">
                                            <img src="'.Html::encode($m->ruta).'"><H4>'.Html::encode($m->nombre).'</H4></div>';
                        }
                        $contador++;
                    }
    
    $coord = new LatLng(['lat' => Html::encode($model->latitud), 'lng' => Html::encode($model->longitud)]);
    
    $marker = new Marker([
        'position' => $coord,
        'title' => Html::encode($model->nombre),
        'icon'=>$imagen    
    ]);

    $marker->attachInfoWindow(
    new InfoWindow([
        'content' => '<p><h3>LUGARES CERCANOS A LA CONSTRUCCION '.Html::encode($model->nombre).'</h3></p>'
        . '<div class="container">
    <div id="main_area">
        <!-- Slider -->
        <div class="row">
            <div class="col-sm-4" id="slider-thumbs">
                <!-- Bottom switcher of slider -->
                <ul class="hide-bullets">'.$imagenesP.'
                </ul>
            </div>
            <div class="col-sm-6">
                <div class="col-xs-12" id="slider">
                    <!-- Top part of the slider -->
                    <div class="row">
                        <div class="col-sm-12" id="carousel-bounding-box">
                            <div class="carousel slide" id="myCarousel">
                                <!-- Carousel items -->
                                <div class="carousel-inner">
                                    '.$imagenesN.'
                                </div>
                                <!-- Carousel nav -->
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Slider-->
        </div>

    </div>
</div>'
        .Html::a('VER TODA LA INFORMACION COMPLETA DE LA CONSTRUCCION', ['view', 'id' => $model->id])
    ])
);


    $map->addOverlay($marker);
}
// Display the map -finally :)
echo $map->display();
?>
</div>
<script>
  jQuery(document).ready(function($) {
 
        $('#myCarousel').carousel({
                interval: 500
        });
 
        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click(function () {
        var id_selector = $(this).attr("id");
        try {
            var id = /-(\d+)$/.exec(id_selector)[1];
            console.log(id_selector, id);
            jQuery('#myCarousel').carousel(parseInt(id));
        } catch (e) {
            console.log('Regex failed!', e);
        }
    });
        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
                 var id = $('.item.active').data('slide-number');
                $('#carousel-text').html($('#slide-content-'+id).html());
        });
});

</script>