<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConstruccionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Construcciones (Lista)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="construccion-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Agregar Construcción', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Ver mapa de las construcciones', ['maps'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nombre',
            'clave',
            'delegacion',
            'colonia',
            [
                'label' => 'Caracteristicas',
                'format' => 'raw',
                'attribute' => 'caracteristicasTexto',
                'value' => function ($data) {
                    return Html::a($data['caracteristicasTexto'], ["/caracteristica/index?idConstruccion=".$data['id']]);
                },

        
	    ],
            [
                'label' => 'Ubicaciones cercanas',
                'format' => 'raw',
                'attribute' => 'galeriasTexto',
                'value' => function ($data) {
                    return Html::a($data['galeriasTexto'], ["/galeria/index?idConstruccion=".$data['id']]);
                },

        
	    ],
            //'calle',
            //'latitud',
            //'longitud',
            //'created_at',
            //'modified_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
