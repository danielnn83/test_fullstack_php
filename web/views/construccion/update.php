<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Construccion */

$this->title = 'Actualizar Construcción: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Construcciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="construccion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
